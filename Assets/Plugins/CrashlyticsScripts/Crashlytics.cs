﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;


public class Crashlytics : MonoBehaviour {

	void Awake () {
		// Comment this line to disable Crashlytics for Android
		Crashlytics.StartWithApiKey ();

		AppDomain.CurrentDomain.UnhandledException += HandleException;	

#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6
		Application.RegisterLogCallback(HandleLog);
#else
		Application.logMessageReceived += HandleLog;
#endif
	}

#if UNITY_IOS && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern void crash();
	
	[DllImport("__Internal")]
	private static extern void startWithApiKey (string apiKey, string developmentPlatformVersion);

	[DllImport("__Internal")]
	private static extern void recordCustomExceptionName (string name, string reason, string stackTrace);

	[DllImport("__Internal")]
	private static extern void setDebugMode (int debugMode);
#endif

	public static void SetDebugMode (bool debugMode)
	{
#if UNITY_IOS && !UNITY_EDITOR
		setDebugMode(debugMode ? 1 : 0);
#endif
	}
	
	public static void Crash () {
#if UNITY_IOS && !UNITY_EDITOR
		crash ();
#elif UNITY_ANDROID && !UNITY_EDITOR
		var crashlyticsClass = new AndroidJavaClass ("com.crashlytics.android.Crashlytics");
		var crashlyticsInstance = crashlyticsClass.CallStatic<AndroidJavaObject>("getInstance");
		crashlyticsInstance.Call ("crash");
#endif
	}
	
	public static void StartWithApiKey () {
#if UNITY_ANDROID && !UNITY_EDITOR
		StartAndroid ();
#endif
	}

#if UNITY_IOS && !UNITY_EDITOR
	[Obsolete("StartWithApiKeyiOS is deprecated. Crashlytics is initialized at launch with no additional calls.")]
	static void StartWithApiKeyiOS (string apiKey) {}

#elif UNITY_ANDROID && !UNITY_EDITOR
	static void StartAndroid () {
		// Fabric.with(UnityPlayer.currentActivity(), new Crashlytics());

		var playerClass = AndroidJNI.FindClass ("com/unity3d/player/UnityPlayer");
		var activityField = AndroidJNI.GetStaticFieldID (playerClass, "currentActivity", "Landroid/app/Activity;");
		var activityObj = AndroidJNI.GetStaticObjectField (playerClass, activityField);
		
		var crashClass = AndroidJNI.FindClass ("com/crashlytics/android/Crashlytics");
		var crashConstructor = AndroidJNI.GetMethodID (crashClass, "<init>", "()V");
		var crashObj = AndroidJNI.NewObject (crashClass, crashConstructor, new jvalue[0]);
		
		var fabricClass = AndroidJNI.FindClass ("io/fabric/sdk/android/Fabric");
		var withMethodId = AndroidJNI.GetStaticMethodID (fabricClass, "with", "(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;");

		jvalue[] argsArray = new jvalue[2];
		argsArray [0].l = activityObj;
		argsArray [1].l = AndroidJNI.NewObjectArray(1, AndroidJNI.FindClass ("com/crashlytics/android/Crashlytics"), crashObj);
		AndroidJNI.CallStaticObjectMethod (fabricClass, withMethodId, argsArray);
	}
#endif

	private static void RecordCustomExceptionName (string name, string reason, string stackTrace)
	{
#if UNITY_IOS && !UNITY_EDITOR
		recordCustomExceptionName (name, reason, stackTrace);
#endif
	}

	static void HandleException(object sender, UnhandledExceptionEventArgs eArgs) {
		Exception e = (Exception)eArgs.ExceptionObject;
		HandleLog (e.Message.ToString (), e.StackTrace.ToString (), LogType.Exception);
	}

	static void HandleLog(string message, string stackTraceString, LogType type) {
		if (type == LogType.Exception) {
#if UNITY_IOS && !UNITY_EDITOR
			HandleExceptioniOS (message, stackTraceString);
#elif UNITY_ANDROID && !UNITY_EDITOR
			HandleExceptionAndroid (message, stackTraceString);
#endif
		}
	}

#if UNITY_IOS && !UNITY_EDITOR
	static void HandleExceptioniOS (string message, string stackTraceString) {
		string[] messageParts = getMessageParts(message);

		RecordCustomExceptionName(messageParts[0], messageParts[1], stackTraceString);
	}

	private static string[] getMessageParts (string message) {
		// Split into two parts so we only split on the first delimiter
		char[] delim = { ':' };
		string[] messageParts = message.Split(delim, 2, StringSplitOptions.None);

		foreach (string part in messageParts)
			part.Trim ();

		if (messageParts.Length == 2) {
			return messageParts;
		} else {
			return new string[] {"Exception", message};
		}
	}

#elif UNITY_ANDROID && !UNITY_EDITOR
	static void HandleExceptionAndroid (string message, string stackTraceString) {
		// new Exception(message)
		var exceptionClass = AndroidJNI.FindClass("java/lang/Exception");
		var exceptionConstructor = AndroidJNI.GetMethodID (exceptionClass, "<init>", "(Ljava/lang/String;)V");		
		var exceptionArgs = new jvalue[1];
		exceptionArgs[0].l = AndroidJNI.NewStringUTF(message);
		var exceptionObj = AndroidJNI.NewObject (exceptionClass, exceptionConstructor, exceptionArgs);

		// stackTrace = [StackTraceElement, ...]
		var stackTraceElClass = AndroidJNI.FindClass ("java/lang/StackTraceElement");
		var stackTraceElConstructor = AndroidJNI.GetMethodID (stackTraceElClass, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V");
		var parsedStackTrace = ParseStackTraceString (stackTraceString);
		var stackTraceArray = AndroidJNI.NewObjectArray(parsedStackTrace.Length, stackTraceElClass, IntPtr.Zero);
		for (var i = 0; i < parsedStackTrace.Length; i++) {
			var frame = parsedStackTrace[i];

			// new StackTraceElement()
			var stackTraceArgs = new jvalue[4];
			stackTraceArgs[0].l = AndroidJNI.NewStringUTF(frame["class"]);
			stackTraceArgs[1].l = AndroidJNI.NewStringUTF(frame["method"]);
			stackTraceArgs[2].l = AndroidJNI.NewStringUTF(frame["file"]);
			stackTraceArgs[3].i = Int32.Parse(frame["line"]);
			var stackTraceEl = AndroidJNI.NewObject (stackTraceElClass, stackTraceElConstructor, stackTraceArgs);
			AndroidJNI.SetObjectArrayElement(stackTraceArray, i, stackTraceEl);
		}

		// exception.setStackTrace(stackTraceArray)
		var setStackTraceMethod = AndroidJNI.GetMethodID (exceptionClass, "setStackTrace", "([Ljava/lang/StackTraceElement;)V");
		var setStackTraceArgs = new jvalue[1];
		setStackTraceArgs[0].l = stackTraceArray;
		AndroidJNI.CallVoidMethod (exceptionObj, setStackTraceMethod, setStackTraceArgs);

		// Crashlytics.logException(exception)
		var crashClass = AndroidJNI.FindClass ("com/crashlytics/android/Crashlytics");
		var logExceptionMethod = AndroidJNI.GetStaticMethodID (crashClass, "logException", "(Ljava/lang/Throwable;)V");
		var logExceptionArgs = new jvalue[1];
		logExceptionArgs[0].l = exceptionObj;
		AndroidJNI.CallStaticVoidMethod (crashClass, logExceptionMethod, logExceptionArgs);

		Log ("Saved exception: " + message);
	}
#endif

	static Dictionary<string, string>[] ParseStackTraceString (string stackTraceString) {
		string[] splitStackTrace = stackTraceString.Split(Environment.NewLine.ToCharArray());
		var result = new List< Dictionary<string, string> >();

		foreach (var frameString in splitStackTrace) {
			var regex = @"(.+)\.(.+) \((.*)\)";
			var matches = Regex.Matches(frameString, regex);

			if (matches.Count != 1) {
				continue;
			}

			var match = matches[0];
			var dict = new Dictionary<string, string>();
			dict.Add("class", match.Groups[1].Value);
			dict.Add("method", match.Groups[2].Value + " (" + match.Groups[3].Value + ")");
			dict.Add("file", match.Groups[1].Value);
			dict.Add("line", "0");

			result.Add (dict);
		}

		return result.ToArray();
	}
	
	static void Log (string s) {
#if UNITY_ANDROID && !UNITY_EDITOR
		var logClass = new AndroidJavaClass("android.util.Log");
		logClass.CallStatic<int> ("e", "Crashlytics", s);
#else
		Debug.Log ("[Crashlytics] " + s);
#endif
	}
}
