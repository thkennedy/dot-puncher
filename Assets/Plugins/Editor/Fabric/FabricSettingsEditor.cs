﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

public class FabricSettingsEditor : EditorWindow {

	private static FabricSettingsEditor instance;
	private static FabricSettings settings;

	private static int windowHeight = 534;
	private static int windowWidth = 322;
	private static int iconHeight = 99;
	private static int iconWidth = 95;

	[MenuItem("Fabric/Fabric Settings")]
	public static void Init ()
	{
		instance = GetFabricEditorWindow () as FabricSettingsEditor;
		settings = FabricSettings.Instance;
	}

	public void OnGUI ()
	{
		if (!instance)
			instance = GetFabricEditorWindow () as FabricSettingsEditor;

		if (!settings)
			settings = FabricSettings.Instance;

		// Background texture
		Texture2D fabricBackgroundTexture = AssetDatabase.LoadAssetAtPath("Assets/Plugins/Editor/Fabric/fabric-background.png",
		                                                                  typeof(Texture2D)) as Texture2D;

		if (fabricBackgroundTexture)
			EditorGUI.DrawTextureTransparent(new Rect(0, 0, windowWidth, windowHeight), fabricBackgroundTexture);

		// Fabric icon
		Texture2D fabricIconTexture = AssetDatabase.LoadAssetAtPath("Assets/Plugins/Editor/Fabric/fabric-icon.png",
		                                            				typeof(Texture2D)) as Texture2D;

		if (fabricIconTexture) {
			GUIStyle fabricIconTextureStyle = new GUIStyle();
			fabricIconTextureStyle.fixedHeight = iconHeight;
			fabricIconTextureStyle.fixedWidth = iconWidth;
			fabricIconTextureStyle.margin.left = (int)(instance.position.width / 2 - iconWidth / 2);
			GUILayout.Label(fabricIconTexture, fabricIconTextureStyle);
		}

		GUIStyle fabricTextStyle = new GUIStyle ();
		if (fabricBackgroundTexture) {
			// If we have our dark background, use a white text color
			fabricTextStyle.normal.textColor = Color.white;
		}
		
		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Fabric API Key", fabricTextStyle);
		settings.ApiKey = EditorGUILayout.TextField(settings.ApiKey);

		EditorGUILayout.Space ();

		EditorGUILayout.LabelField("Fabric Build Secret", fabricTextStyle);
		settings.BuildSecret = EditorGUILayout.TextField(settings.BuildSecret);

		EditorGUILayout.Space ();

		GUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField("Enable Crashlytics (iOS)", fabricTextStyle, GUILayout.Width(130));
		settings.EnableCrashlytics = EditorGUILayout.Toggle(settings.EnableCrashlytics);
		GUILayout.EndHorizontal ();

		EditorGUILayout.LabelField("Crashlytics for Android can be disabled in Crashlytics.cs", fabricTextStyle);

		EditorGUILayout.Space ();

		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button("Setup Fabric", GUILayout.Width(100))) {
			FabricSetup.SetScriptExecutionOrder ();
			FabricSetup.PrepareAndroidManifest ();
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
	}

	private static EditorWindow GetFabricEditorWindow ()
	{
		return EditorWindow.GetWindowWithRect(typeof(FabricSettingsEditor),
		                               		  new Rect(100, 100, windowWidth, windowHeight),
		                               		  false,						// false = tab-able, true = window
		                               		  "Fabric");
	}
}
