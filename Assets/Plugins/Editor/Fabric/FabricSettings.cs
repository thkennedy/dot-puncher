using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public class FabricSettings : ScriptableObject
{
	const string fabricSettingsAssetName = "FabricSettings";
	const string fabricSettingsPath = "Editor Default Resources";
	const string fabricSettingsAssetExtension = "asset";

	private static FabricSettings instance;

	public static FabricSettings Instance
	{
		get
		{
			if (instance == null)
			{
				string assetNameWithExtension = string.Join (".", new string[]{fabricSettingsAssetName, fabricSettingsAssetExtension});
				instance = EditorGUIUtility.Load (assetNameWithExtension) as FabricSettings;
				if (instance == null)
				{
					// resource not found
					instance = CreateInstance<FabricSettings> ();
					string dir = Path.Combine (Application.dataPath, fabricSettingsPath);
					if (!Directory.Exists (dir))
					{
						AssetDatabase.CreateFolder ("Assets", fabricSettingsPath);
					}

					string assetPath = Path.Combine (Path.Combine ("Assets", fabricSettingsPath), assetNameWithExtension);

					AssetDatabase.CreateAsset (instance, assetPath);
				}
			}
			return instance;
		}
	}

	[SerializeField]
	private string FABApiKey;

	[HideInInspector]
	public string ApiKey
	{
		get
		{
			return Instance.FABApiKey;
		}
		set
		{
			Instance.FABApiKey = value;
			EditorUtility.SetDirty(Instance);
		}
	}

	[SerializeField]
	private string FABBuildSecret;

	[HideInInspector]
	public string BuildSecret
	{
		get
		{
			return Instance.FABBuildSecret;
		}
		set
		{
			Instance.FABBuildSecret = value;
			EditorUtility.SetDirty(Instance);
		}
	}

	[SerializeField]
	private bool FABEnableCrashlytics = true;

	[HideInInspector]
	public bool EnableCrashlytics
	{
		get
		{
			return Instance.FABEnableCrashlytics;
		}
		set
		{
			Instance.FABEnableCrashlytics = value;
			EditorUtility.SetDirty(Instance);
		}
	}

}