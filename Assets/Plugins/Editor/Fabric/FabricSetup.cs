﻿using UnityEditor;
using System.IO;
using UnityEngine;
using System.Xml;

//
// Contains setup methods required for Fabric pre-Unity build
//
public class FabricSetup {

	public static void SetScriptExecutionOrder () {
		string crashlyticsLib = typeof (Crashlytics).Name;
		
		foreach (MonoScript script in MonoImporter.GetAllRuntimeMonoScripts()) {
			if (script.name == crashlyticsLib) {
				if (MonoImporter.GetExecutionOrder(script) != -100) {
					FabricUtils.Log ("Changing script execution order for Crashlytics");
					MonoImporter.SetExecutionOrder(script, -100);
				}
			}
		}
	}
	
	//
	// Android
	//
	
	static string outputManifestRelativePath = "Plugins/Android/Crashlytics/AndroidManifest.xml";
	static string outputManifestPath = Path.Combine(Application.dataPath, outputManifestRelativePath);
	
	public static void PrepareAndroidManifest () {
		FabricSetup.BootstrapManifest ();
		FabricSetup.InjectAPIKeyIntoManifest ();
	}
	
	static void BootstrapManifest() {
		var inputManifestPath = Path.Combine(Application.dataPath, "Plugins/Android/Crashlytics/Template-AndroidManifest.xml");
		
		if (!File.Exists (outputManifestPath)) {
			FabricUtils.Log ("Creating " + outputManifestRelativePath);
			File.Copy(inputManifestPath, outputManifestPath);
		}
	}
	
	static void InjectAPIKeyIntoManifest () {
		const string fabricApiKeyName = "io.fabric.ApiKey";
		FabricSettings settings = FabricSettings.Instance;
		
		XmlDocument doc = new XmlDocument();
		
		doc.Load(outputManifestPath);
		if (doc == null) {
			FabricUtils.Error ("Could not open " + outputManifestPath);
			return;
		}
		
		var applicationNodes = doc.GetElementsByTagName("application");
		if (applicationNodes.Count < 1) {
			FabricUtils.Error ("Could not find <application> tag in " + outputManifestPath);
			return;
		}
		
		var applicationNode = applicationNodes [0];
		var androidNs = applicationNode.GetNamespaceOfPrefix("android");
		
		var metaElements = doc.GetElementsByTagName("meta-data");
		foreach (XmlElement metaElement in metaElements) {
			if (metaElement.GetAttribute("name", androidNs) == fabricApiKeyName) {
				if (metaElement.GetAttribute("value", androidNs) == settings.ApiKey) {
					return;
				} else {
					metaElement.ParentNode.RemoveChild(metaElement);
				}
			}
		}
		
		FabricUtils.Log ("Adding API key to " + outputManifestRelativePath);
		
		var metaNode = doc.CreateElement("meta-data");
		metaNode.SetAttribute("name", androidNs, fabricApiKeyName);
		metaNode.SetAttribute("value", androidNs, settings.ApiKey);
		applicationNode.AppendChild (metaNode);
		
		doc.Save (outputManifestPath);
	}
	
}