﻿using UnityEngine;

public static class FabricUtils {

	public static void Log (string message) {
		Debug.Log ("[Fabric] " + message);
	}

	public static void Warn (string message) {
		Debug.LogWarning ("[Fabric] " + message);
	}

	public static void Error (string message) {
		Debug.LogError ("[Fabric] " + message);
	}
}
