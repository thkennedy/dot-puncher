info face="Earth Kid" size=88 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=1,1,1,1 spacing=1,1 outline=0
common lineHeight=88 base=71 scaleW=512 scaleH=512 pages=2 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0
page id=0 file="EarthKid2x_0.png"
page id=1 file="EarthKid2x_1.png"
chars count=190
char id=32   x=477   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=33   x=502   y=91    width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=34   x=506   y=91    width=3     height=90    xoffset=-1    yoffset=-1    xadvance=28    page=0  chnl=15
char id=35   x=507   y=273   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=36   x=401   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=37   x=405   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=70    page=0  chnl=15
char id=38   x=409   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=0  chnl=15
char id=39   x=413   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=15    page=0  chnl=15
char id=40   x=417   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=0  chnl=15
char id=41   x=421   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=0  chnl=15
char id=42   x=425   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=31    page=0  chnl=15
char id=43   x=429   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=0  chnl=15
char id=44   x=433   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=45   x=437   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=0  chnl=15
char id=46   x=441   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=47   x=445   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=48   x=449   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=49   x=453   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=50   x=457   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=51   x=461   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=52   x=465   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=53   x=469   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=54   x=473   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=55   x=92    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=56   x=481   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=57   x=485   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=0  chnl=15
char id=58   x=489   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=59   x=493   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=0  chnl=15
char id=60   x=497   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=0  chnl=15
char id=61   x=501   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=0  chnl=15
char id=62   x=505   y=364   width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=0  chnl=15
char id=63   x=124   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=64   x=128   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=80    page=1  chnl=15
char id=65   x=360   y=0     width=55    height=90    xoffset=0     yoffset=-1    xadvance=55    page=0  chnl=15
char id=66   x=185   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=67   x=268   y=364   width=37    height=90    xoffset=2     yoffset=-1    xadvance=42    page=0  chnl=15
char id=68   x=231   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=69   x=0     y=364   width=44    height=90    xoffset=2     yoffset=-1    xadvance=47    page=0  chnl=15
char id=70   x=45    y=364   width=44    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=71   x=224   y=364   width=43    height=90    xoffset=3     yoffset=-1    xadvance=48    page=0  chnl=15
char id=72   x=144   y=182   width=47    height=90    xoffset=2     yoffset=-1    xadvance=51    page=0  chnl=15
char id=73   x=383   y=364   width=17    height=90    xoffset=2     yoffset=-1    xadvance=22    page=0  chnl=15
char id=74   x=341   y=364   width=23    height=90    xoffset=2     yoffset=-1    xadvance=27    page=0  chnl=15
char id=75   x=56    y=91    width=55    height=90    xoffset=2     yoffset=-1    xadvance=58    page=0  chnl=15
char id=76   x=475   y=182   width=34    height=90    xoffset=2     yoffset=-1    xadvance=39    page=0  chnl=15
char id=77   x=62    y=0     width=61    height=90    xoffset=2     yoffset=-1    xadvance=65    page=0  chnl=15
char id=78   x=48    y=182   width=47    height=90    xoffset=2     yoffset=-1    xadvance=52    page=0  chnl=15
char id=79   x=323   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=80   x=334   y=182   width=46    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=81   x=415   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=82   x=381   y=182   width=46    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=83   x=287   y=182   width=46    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=84   x=332   y=91    width=48    height=90    xoffset=2     yoffset=-1    xadvance=52    page=0  chnl=15
char id=85   x=430   y=91    width=47    height=90    xoffset=2     yoffset=-1    xadvance=53    page=0  chnl=15
char id=86   x=248   y=0     width=55    height=90    xoffset=0     yoffset=-1    xadvance=56    page=0  chnl=15
char id=87   x=0     y=0     width=61    height=90    xoffset=2     yoffset=-1    xadvance=65    page=0  chnl=15
char id=88   x=167   y=91    width=54    height=90    xoffset=2     yoffset=-1    xadvance=59    page=0  chnl=15
char id=89   x=112   y=91    width=54    height=90    xoffset=2     yoffset=-1    xadvance=57    page=0  chnl=15
char id=90   x=47    y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=91   x=236   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=92   x=240   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=93   x=244   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=94   x=248   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=37    page=1  chnl=15
char id=95   x=252   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=96   x=256   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=97   x=416   y=0     width=55    height=90    xoffset=0     yoffset=-1    xadvance=55    page=0  chnl=15
char id=98   x=139   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=99   x=472   y=0     width=37    height=90    xoffset=2     yoffset=-1    xadvance=42    page=0  chnl=15
char id=100  x=277   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=101  x=135   y=364   width=44    height=90    xoffset=2     yoffset=-1    xadvance=47    page=0  chnl=15
char id=102  x=90    y=364   width=44    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=103  x=180   y=364   width=43    height=90    xoffset=3     yoffset=-1    xadvance=48    page=0  chnl=15
char id=104  x=192   y=182   width=47    height=90    xoffset=2     yoffset=-1    xadvance=51    page=0  chnl=15
char id=105  x=365   y=364   width=17    height=90    xoffset=2     yoffset=-1    xadvance=22    page=0  chnl=15
char id=106  x=478   y=91    width=23    height=90    xoffset=2     yoffset=-1    xadvance=27    page=0  chnl=15
char id=107  x=0     y=91    width=55    height=90    xoffset=2     yoffset=-1    xadvance=58    page=0  chnl=15
char id=108  x=306   y=364   width=34    height=90    xoffset=2     yoffset=-1    xadvance=39    page=0  chnl=15
char id=109  x=186   y=0     width=61    height=90    xoffset=2     yoffset=-1    xadvance=65    page=0  chnl=15
char id=110  x=96    y=182   width=47    height=90    xoffset=2     yoffset=-1    xadvance=52    page=0  chnl=15
char id=111  x=461   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=112  x=0     y=273   width=46    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=113  x=369   y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=114  x=240   y=182   width=46    height=90    xoffset=2     yoffset=-1    xadvance=49    page=0  chnl=15
char id=115  x=428   y=182   width=46    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=116  x=381   y=91    width=48    height=90    xoffset=2     yoffset=-1    xadvance=52    page=0  chnl=15
char id=117  x=0     y=182   width=47    height=90    xoffset=2     yoffset=-1    xadvance=53    page=0  chnl=15
char id=118  x=304   y=0     width=55    height=90    xoffset=0     yoffset=-1    xadvance=56    page=0  chnl=15
char id=119  x=124   y=0     width=61    height=90    xoffset=2     yoffset=-1    xadvance=65    page=0  chnl=15
char id=120  x=277   y=91    width=54    height=90    xoffset=2     yoffset=-1    xadvance=59    page=0  chnl=15
char id=121  x=222   y=91    width=54    height=90    xoffset=2     yoffset=-1    xadvance=57    page=0  chnl=15
char id=122  x=93    y=273   width=45    height=90    xoffset=2     yoffset=-1    xadvance=50    page=0  chnl=15
char id=123  x=364   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=124  x=368   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=20    page=1  chnl=15
char id=125  x=372   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=126  x=376   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=1  chnl=15
char id=160  x=380   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=161  x=384   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=162  x=388   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=163  x=392   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=165  x=396   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=166  x=400   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=20    page=1  chnl=15
char id=167  x=404   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=168  x=408   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=169  x=412   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=58    page=1  chnl=15
char id=170  x=416   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=29    page=1  chnl=15
char id=171  x=420   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=172  x=424   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=1  chnl=15
char id=173  x=428   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=174  x=360   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=58    page=1  chnl=15
char id=175  x=356   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=176  x=352   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=32    page=1  chnl=15
char id=177  x=348   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=43    page=1  chnl=15
char id=178  x=344   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=179  x=340   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=180  x=336   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=181  x=332   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=45    page=1  chnl=15
char id=182  x=328   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=42    page=1  chnl=15
char id=183  x=324   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=184  x=320   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=185  x=316   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=26    page=1  chnl=15
char id=186  x=312   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=29    page=1  chnl=15
char id=187  x=308   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=188  x=304   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=66    page=1  chnl=15
char id=189  x=300   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=66    page=1  chnl=15
char id=190  x=296   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=66    page=1  chnl=15
char id=191  x=292   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=48    page=1  chnl=15
char id=192  x=288   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=193  x=284   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=194  x=280   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=195  x=276   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=196  x=272   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=197  x=268   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=198  x=264   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=79    page=1  chnl=15
char id=199  x=260   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=200  x=232   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=201  x=228   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=202  x=224   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=203  x=220   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=204  x=216   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=205  x=212   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=206  x=208   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=207  x=204   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=208  x=200   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=209  x=196   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=210  x=192   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=211  x=188   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=212  x=184   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=213  x=180   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=214  x=176   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=215  x=172   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=46    page=1  chnl=15
char id=216  x=168   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=61    page=1  chnl=15
char id=217  x=164   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=218  x=160   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=219  x=156   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=220  x=152   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=57    page=1  chnl=15
char id=221  x=148   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=222  x=144   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=53    page=1  chnl=15
char id=223  x=140   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=48    page=1  chnl=15
char id=224  x=136   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=225  x=132   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=226  x=120   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=227  x=116   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=228  x=112   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=229  x=108   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=230  x=104   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=70    page=1  chnl=15
char id=231  x=100   y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=39    page=1  chnl=15
char id=232  x=96    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=233  x=88    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=234  x=84    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=235  x=80    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=236  x=76    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=237  x=72    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=238  x=68    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=239  x=64    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=22    page=1  chnl=15
char id=240  x=60    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=241  x=56    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=242  x=52    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=243  x=48    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=244  x=44    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=245  x=40    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=246  x=36    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=247  x=32    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=43    page=1  chnl=15
char id=248  x=28    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=48    page=1  chnl=15
char id=249  x=24    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=250  x=20    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=251  x=16    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=252  x=12    y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=253  x=8     y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=39    page=1  chnl=15
char id=254  x=4     y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=44    page=1  chnl=15
char id=255  x=0     y=0     width=3     height=90    xoffset=-1    yoffset=-1    xadvance=39    page=1  chnl=15
