﻿using UnityEngine;
using System.Collections;
using System;

public static class UserData
{
    public static string playerUID
    {
        get
        {
            if (!PlayerPrefs.HasKey("playerUID"))
                PlayerPrefs.SetString("playerUID", Guid.NewGuid().ToString());

                return PlayerPrefs.GetString("playerUID");   
        }
    }
    public static bool isFacebookLinked
    {
        get
        {
            return GetBool("isFacebookLinked");
        }

        set
        {
            SetBool("isFacebookLinked", value);
        }
    }

    public static bool hasPlayedBefore
    {
        get
        {
            return GetBool("hasPlayedBefore");
        }

        set
        {
            SetBool("hasPlayedBefore", value);
        }
    }

    public static string playerFBID
    {
        get
        {
            return PlayerPrefs.GetString("playerFBID");
        }

        set 
        {
            PlayerPrefs.SetString("playerFBID", value);
        }
    }

    public static string playerFirstName
    {
        get 
        {
            return PlayerPrefs.GetString("playerFirstName");
        }

        set
        {
            PlayerPrefs.SetString("playerFirstName", value);
        }
    }

    public static string playerLastName
    {
        get
        {
            return PlayerPrefs.GetString("playerLastName");
        }

        set
        {
            PlayerPrefs.SetString("playerLastName", value);
        }
    }

    public static string playerFullName
    {
        get
        {
            if (PlayerPrefs.HasKey("playerFullName"))
            {
                return PlayerPrefs.GetString("playerFullName");
            }
            
            return "Masked Fighter";
            
        }

        set
        {
            PlayerPrefs.SetString("playerFullName", value);
        }
    }

    public static string playerFBLink
    {
        get
        {
            return PlayerPrefs.GetString("playerFBLink");
        }

        set
        {
            PlayerPrefs.SetString("playerFBLink", value);
        }
    }

    public static int playerTimeZone
    {
        get
        {
            return PlayerPrefs.GetInt("playerTimeZone");
        }

        set
        {
            PlayerPrefs.SetInt("playerTimeZone", value);
        }
    }

    public static string playerFBPicURL
    {
        get
        {
            return PlayerPrefs.GetString("playerFBPicURL");
        }

        set
        {
            PlayerPrefs.SetString("playerFBPicURL", value);
        }
    }

    public static string playerShortName
    {
        get
        {
            if (!PlayerPrefs.HasKey("playerShortName"))
                PlayerPrefs.SetString("playerShortName", "Masked Fighter");

            return PlayerPrefs.GetString("playerShortName");
        }

        set
        {
            PlayerPrefs.SetString("playerShortName", value);
        }
    }

    public static bool soundIsOn
    {
        get
        {
            // be default sound should be on.
            if (!PlayerPrefs.HasKey("SoundIsOn"))
                SetBool("SoundIsOn", true);

            return GetBool("SoundIsOn");
        }

        set
        {
            SetBool("SoundIsOn", value);
        }
    }

    public static int difficulty
    {
        get
        {
            if (!PlayerPrefs.HasKey("AIDifficulty"))
                PlayerPrefs.SetInt("AIDifficulty", 0);

            return PlayerPrefs.GetInt("AIDifficulty");
        }

        set
        {
            PlayerPrefs.SetInt("AIDifficulty", value);

            if (PlayerPrefs.GetInt("AIDifficulty") > 3)
                PlayerPrefs.SetInt("AIDifficulty", 0);
        }
    }

    public static Vector4[] playerColors
    {
        get
        {
            // init colors if we don't have any yet
            if (!PlayerPrefs.HasKey("PlayerColors0"))
            {
                InitPlayerColors();
            }

            Vector4[] returnArray = new Vector4[8];
            for(int i = 0; i < 8; ++i)
            {
                GetVec4("PlayerColors" + i.ToString(), ref returnArray[i]);
            }

            return returnArray;
        }

        set
        {
            for(int i = 0; i < 8; ++i)
            {
                SetVec4("PlayerColors" + i.ToString(), value[i]);
            }
        }
    }

    private static void InitPlayerColors()
    {
        // green
        SetVec4("PlayerColors0", new Vector4(0, 255, 0, 255));
        // red
        SetVec4("PlayerColors1", new Vector4(255, 0, 0, 255));
        // blue
        SetVec4("PlayerColors2", new Vector4(0, 0, 255, 255));
        // yellow
        SetVec4("PlayerColors3", new Vector4(255, 255, 0, 255));
        // purple
        SetVec4("PlayerColors4", new Vector4(255, 0, 255, 255));
        // cyan(ish)
        SetVec4("PlayerColors5", new Vector4(0, 255, 255, 255));
        // white
        SetVec4("PlayerColors6", new Vector4(255, 255, 255, 255));
        // black
        SetVec4("PlayerColors7", new Vector4(0, 0, 0, 255));
    }

    public static void RemoveFacebookData()
    {
        isFacebookLinked = false;
        playerFBID = "";
        playerFBLink = "";
        playerFBPicURL = "";
        playerFirstName = "";
        playerLastName = "";
        playerFullName = "";
        playerShortName = "Masked Fighter";
        
    }

    private static void SetBool(string key, bool value)
    {
        PlayerPrefs.SetInt(key, Convert.ToInt32(value));
    }
    private static bool GetBool(string key)
    {
        return Convert.ToBoolean(PlayerPrefs.GetInt(key));
    }

    private static void SetVec4(string key, Vector4 vector)
    {
        PlayerPrefs.SetFloat(key + "x", vector.x);
        PlayerPrefs.SetFloat(key + "y", vector.y);
        PlayerPrefs.SetFloat(key + "z", vector.z);
        PlayerPrefs.SetFloat(key + "w", vector.w);
    }

    private static void GetVec4(string key, ref Vector4 returnVec)
    {
        returnVec.x = PlayerPrefs.GetFloat(key + "x");
        returnVec.y = PlayerPrefs.GetFloat(key + "y");
        returnVec.z = PlayerPrefs.GetFloat(key + "z");
        returnVec.w = PlayerPrefs.GetFloat(key + "w");
        
    }
   
}
