﻿using FullInspector;
using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using Object = UnityEngine.Object;

namespace DotFighter
{
    public enum GameVersion
    {
        FREEMIUM_IOS,
        PREMIUM_IOS,
        FREEMIUM_ANDROID
    }
    public class GameController : BaseBehavior<NullSerializer>
    {

        public const GameVersion GAME_VERSION = GameVersion.PREMIUM_IOS;

        #region Delegates
        public delegate void GameControllerDelegateNoParams(Object sender = null);

        public GameControllerDelegateNoParams GameStarted;
        public GameControllerDelegateNoParams GameEnded;
        public GameControllerDelegateNoParams PlayerListUpdated;

        #endregion

        #region Member Vars
        public Dictionary<string, string> players = new Dictionary<string, string>();

        public Dictionary<string, int> playerColorIndexes = new Dictionary<string, int>();

        public Dictionary<string, int> playerScores = new Dictionary<string, int>();

        public List<Color> playerColors;

        public int secondsToWaitForPlayers = 30;

        public float joinTimeLeft;

        public int gameLength = 30;

        private int gameTimeLeft;

        public RectTransform gameTimerControl;

        public Text gameTimerText;

        public RectTransform endGameControl;

        public Text endGameText;

        public int maxPlayers = 8;

        public string gameType = "Dot Fighter Base";

        public RectTransform scoreCalcPanel;

        public int numPlayers
        {
            get
            {
                return players.Count;
            }
        }

        public int rows
        {
            get
            {
                return dotController.rows;
            }
        }

        public int columns
        {
            get
            {
                return dotController.columns;
            }
        }
        #endregion

        #region Controller Accessors
        private static GameController _instance;
        public static GameController instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GameController>();
                }

                return _instance;
            }
        }

        public GameStates gameState;

        private SoundController _soundController;
        public SoundController soundController
        {
            get 
            {
                if (_soundController == null)
                {
                    _soundController = FindObjectOfType<SoundController>();
                    if (_soundController == null)
                    {
                        GameObject go = GameObject.Instantiate(Resources.Load("Prefabs/SoundController")) as GameObject;
                        _soundController = go.GetComponent<SoundController>();
                    } 
                }

                return _soundController;
            }
        }
        
        private DotAI _dotAI = null;
        public DotAI dotAI
        {
            get
            {
                if (_dotAI == null)
                    _dotAI = gameObject.GetComponent<DotAI>();

                return _dotAI;
            }
        }

        private DotController _dotController = null;

        public DotController dotController
        {
            get
            {
                if (_dotController == null)
                    _dotController = gameObject.GetComponent<DotController>();

                return _dotController;
            }
        }

        private ScoreController _scoreController = null;
        public ScoreController scoreController
        {
            get
            {
                if (_scoreController == null)
                    _scoreController = gameObject.GetComponent<ScoreController>();

                return _scoreController;
            }
        }

        private Canvas _screenCanvas;

        public Canvas screenCanvas
        {
            get
            {
                if (_screenCanvas == null)
                    _screenCanvas = FindObjectOfType<Canvas>();

                return _screenCanvas;
            }
        }

        private NetworkController _networkController;

        public NetworkController networkController
        {
            get
            {
                if (_networkController == null)
                    _networkController = GetComponent<NetworkController>();

                return _networkController;
            }
        }

        private ClickDetector _clickDetector;

        public ClickDetector clickDetector
        {
            get
            {
                if (_clickDetector == null)
                    _clickDetector = GetComponent<ClickDetector>();

                return _clickDetector;
            }
        }

        #endregion

        #region Initialization

        // called early
        protected override void Awake()
        {
            //base.Awake();

            if (_instance == null)
            {
                // create the singleton on first awake
                _instance = this;
                //DontDestroyOnLoad(this);
            }
            else
            {
                // if we've accidentally made two game controllers
                if (this != _instance)
                    Destroy(this.gameObject);
            }
        }

        void Start()
        {
            // register for callbacks
            RegisterForNetworkCallback(ServerInit, NetworkMessages.OnServerInitialized);
            RegisterForNetworkCallback(ClientDisconnect, NetworkMessages.OnPlayerDisconnected);
            RegisterForServerEvent(HostListReceived, MasterServerEvent.HostListReceived);
            RegisterForServerEvent(ServerRegistrationComplete, MasterServerEvent.RegistrationSucceeded);
            scoreCalcPanel.gameObject.SetActive(false);

            // UnityAds
            if (Advertisement.isReady() && GAME_VERSION != GameVersion.PREMIUM_IOS) 
            {
                Advertisement.Show(null, new ShowOptions
                {
                    pause = true,
                    resultCallback = result =>
                    {
                        BeginMatchmaking();
                    }
                });
            }
            else
            {
                BeginMatchmaking();
            }

            
        }

        #endregion

        #region Callback Registration

        void ServerInit()
        {
            InitPlayers();
        }

        void ClientDisconnect()
        {
            GetComponent<NetworkView>().RPC("HandleClientDisconnect", RPCMode.All);
        }

        public void RegisterForNetworkCallback(NetworkController.NetworkMessageReceived callback, NetworkMessages messageType)
        {
            networkController.RegisterCallbackForMessage(callback, messageType);
        }

        public void RegisterForServerEvent(NetworkController.MasterServerEventDelegate callback, MasterServerEvent serverEvent)
        {
            networkController.RegisterForServerEvent(callback, serverEvent);
        }
        public void RegisterForGameEventCallback(GameControllerDelegateNoParams callBack, GameEvents gameEvent)
        {
            switch (gameEvent)
            {
                case GameEvents.GameStarted:
                    GameStarted += callBack;
                    break;

                case GameEvents.GameEnded:
                    GameEnded += callBack;
                    break;

                case GameEvents.PlayerListUpdated:
                    PlayerListUpdated += callBack;
                    break;

            }
        }

        #endregion

        #region Gameplay Methods
        public void BeginMatchmaking()
        {
            // if we were to try and join games based on special items that would happen here
            // for now try to join a random room
            networkController.BeginMatchmaking();
        }

        private void HostListReceived(MasterServerEvent result)
        {
            HostData[] list = MasterServer.PollHostList();

            // otherwise, see if we can join any of the games
            foreach (HostData host in list)
            {
                // make sure its dot fighter
                if (host.gameType != gameType)
                    continue;

                // if the game has max players, skip
                if (host.connectedPlayers >= host.playerLimit)
                    continue;

                // games that have started are removed from host lists. so if they have room 
                // and havent started, we can try to connect
                
                Network.Connect(host.guid);
                return;
            }

            // failed to find a joinable game or no one else playing (SAD!!)
            CreateGame();
        }

        public void InitPlayers()
        {
            players.Clear();
            playerScores.Clear();
            playerColorIndexes.Clear();

            // put the user's name in there
            AddPlayer(UserData.playerUID, UserData.playerShortName);

            PlayerListUpdate();
        }

        public void CreateGame()
        {
            networkController.RegisterServer(gameType, 8);
        }

        void ServerRegistrationComplete(MasterServerEvent serverEvent)
        {
            // successfully created server - start wait timer
            Debug.Log("Registration Complete");
        }

        public void SetGameTimerVisibility(bool isVisible)
        {
            gameTimerControl.gameObject.SetActive(isVisible);
        }

        public void SetGameTimerText(string text)
        {
            gameTimerText.text = text;
        }
        public void QuitGame()
        {

            Application.LoadLevel("TitleScene");
        }

        public void PlayAgain()
        {
            Application.LoadLevel("BaseLevel");
        }

        private void AddPlayer(string UID, string name)
        {
            players.Add(UID, name);
            playerScores.Add(UID, 0);


        }

        public void InitConnectedPlayerData(NetworkPlayer player)
        {
            GetComponent<NetworkView>().RPC("InitConnectedPlayers", player, SerializationHelper.SerializeToString(players), SerializationHelper.SerializeToString(playerScores));
        }

        public void PlayerListUpdate()
        {
            // assign colors
            playerColorIndexes.Clear();

            // assign player color indexes
            List<string> playerUIDs = new List<string>(players.Keys);
            for (int i = 0; i < numPlayers; ++i)
            {
                playerColorIndexes.Add(playerUIDs[i], i);
            }

            // call callbacks
            PlayerListUpdated();
        }

        #endregion

        #region RPC Methods

        [RPC]
        public void StartGame()
        {
            // move dots 

            // turn on click detector
            clickDetector.isActive = true;

            gameState = GameStates.GAME_STARTED;

            // for any slots not filled, put dummies in there if we are server send them out
            if (Network.isServer)
            {
                for (int i = numPlayers; i < maxPlayers; ++i)
                {
                    string playerID = Guid.NewGuid().ToString();
                    AddPlayer(playerID, "Robot Opponent");
                    dotAI.AddAI(playerID);

                    // process connected player
                    GetComponent<NetworkView>().RPC("ProcessConnectedClient", RPCMode.Others, playerID, "Masked Fighter");
                }

                PlayerListUpdate();

                // de-register from master server
                //MasterServer.UnregisterHost();
                Network.maxConnections = 0;
            }

            StartCoroutine("GameTimer", gameLength);

            

            // start game callbacks
            if (GameStarted != null)
                GameStarted();
        }

        [RPC]
        public void ClickDot(int claimRow, int claimColumn, string playerUID)
        {
            dotController.ClaimDot(claimRow, claimColumn, playerUID);

        }

        [RPC]
        private void EndGame()
        {
            if (Network.isServer)
            {
                GetComponent<NetworkView>().RPC("SyncDots", RPCMode.All, dotController.GetByteSerializedDots(), SerializationHelper.SerializeToString(playerScores));
            }
        }

        [RPC]
        public void ProcessConnectedClient(string uid, string playerName, NetworkMessageInfo sender)
        {
            // add the person's name to that slot
            AddPlayer(uid, playerName);

            PlayerListUpdate();
        }

        [RPC]
        public void UpdateJoinTime(float newJoinTime)
        {

            joinTimeLeft = newJoinTime;
        }

        [RPC]
        public void InitConnectedPlayers(string sentPlayers, string sentPlayerScores)
        {
            // players already connected to server(including server player)
            Dictionary<string, string> alreadyConnectedPlayers = new Dictionary<string, string>();
            SerializationHelper.DeserializeFromString(sentPlayers, alreadyConnectedPlayers);
            foreach (KeyValuePair<string, string> pair in alreadyConnectedPlayers)
            {
                players.Add(pair.Key, pair.Value);
            }

            Dictionary<string, int> alreadyConnectedPlayerScores = new Dictionary<string, int>();
            SerializationHelper.DeserializeFromString(sentPlayerScores, alreadyConnectedPlayerScores);

            foreach (KeyValuePair<string, int> pair in alreadyConnectedPlayerScores)
            {
                playerScores.Add(pair.Key, pair.Value);
            }
        }

        [RPC]
        public void SyncDots(byte[] serializedDots, string serializedScores)
        {
            DotSerialized[] dotArray = null;

            SerializationHelper.DeserializeFromByteArray(serializedDots, ref dotArray);
            int k = 0;
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    dotController.allDots[i, j].whoClickedMeLast = dotArray[k++].whoClickedMeLast;
                }
            }

            SerializationHelper.DeserializeFromString(serializedScores, playerScores);
        }

        [RPC]
        public void HandleClientDisconnect()
        {
            if (gameState <= GameStates.MATCHMAKING)
            {
                // clear player list and add self
                InitPlayers();

                // send others your deets in process connected client
                GetComponent<NetworkView>().RPC("ProcessConnectedClient", RPCMode.Others, UserData.playerUID, UserData.playerShortName);
            }

        }

        #endregion

        #region Coroutines
        public IEnumerator GameTimer(float gameTime)
        {
            gameTimeLeft = (int)gameTime;

            gameTimerControl.gameObject.SetActive(true);

            clickDetector.isActive = true;

            while (gameTimeLeft >= 0)
            {
                SetGameTimerText(gameTimeLeft.ToString());
                --gameTimeLeft;
                if (Network.isServer)
                    GetComponent<NetworkView>().RPC("SyncDots", RPCMode.Others, dotController.GetByteSerializedDots(), SerializationHelper.SerializeToString(playerScores));
                yield return new WaitForSeconds(1.0f);
            }

            gameState = GameStates.GAME_ENDED;
            // game is over, turn off click dectector for all so no more changes
            clickDetector.isActive = false;

            scoreCalcPanel.gameObject.SetActive(true);

            // score collection wait
            StartCoroutine("ScoreTimer");
        }

        IEnumerator ScoreTimer()
        {
            // bring up a panel to hide calcs
            yield return new WaitForSeconds(1.0f);

            // send final scores and dot claims if I'm the master client
            EndGame();

            yield return new WaitForSeconds(1.0f);

            Network.Disconnect();

            scoreCalcPanel.gameObject.SetActive(false);

            // invoke game ended callback
            if (GameEnded != null)
                GameEnded();
        }



        #endregion

    }
}