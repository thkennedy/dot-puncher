﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace DotFighter
{
    public enum AIDifficulty
    {
        Easy,
        Medium,
        Hard,
        Insane
    }

    public class DotAI : MonoBehaviour
    {
        List<string> bots = new List<string>();

        public float tapSpeedUpper = 0.5f;
        public float tapSpeedLower = 0.1f;

        public AIDifficulty currentDifficulty;

        void Start()
        {
            GameController.instance.RegisterForGameEventCallback(OnGameStart, GameEvents.GameStarted);

            // get difficulty
            UpdateDifficulty();

        }

        void UpdateDifficulty()
        {
            switch (UserData.difficulty)
            {
                case 0:
                    currentDifficulty = AIDifficulty.Easy;
                    tapSpeedUpper = 0.8f;
                    tapSpeedLower = 0.3f;
                    break;
                case 1:
                    currentDifficulty = AIDifficulty.Medium;
                    tapSpeedUpper = 0.5f;
                    tapSpeedLower = 0.2f;
                    break;
                case 2:
                    currentDifficulty = AIDifficulty.Hard;
                    tapSpeedUpper = 0.4f;
                    tapSpeedLower = 0.1f;
                    break;
                case 3:
                    currentDifficulty = AIDifficulty.Insane;
                    tapSpeedUpper = 0.2f;
                    tapSpeedLower = 0.01f;
                    break;
                default:
                    currentDifficulty = AIDifficulty.Easy;
                    tapSpeedUpper = 0.5f;
                    tapSpeedLower = 0.1f;
                    break;
            }
        }

        public int maxRows
        {
            get
            {
                return GameController.instance.rows;
            }
        }

        public int maxColumns
        {
            get
            {
                return GameController.instance.columns;
            }
        }

        public void OnGameStart(Object sender)
        {
            for (int i = 0; i < bots.Count; ++i)
            {
                StartCoroutine("BotClaimDot", bots[i]);
            }
        }

        public void AddAI(string key)
        {
            bots.Add(key);
        }

        public IEnumerator BotClaimDot(string key)
        {
            while(GameController.instance.gameState == GameStates.GAME_STARTED)
            {
                GameController.instance.GetComponent<NetworkView>().RPC("ClickDot", RPCMode.All,UnityEngine.Random.Range(0, maxRows), UnityEngine.Random.Range(0, maxColumns), key);

                yield return new WaitForSeconds(UnityEngine.Random.Range(tapSpeedLower, tapSpeedUpper));
            }
        }


    }

}