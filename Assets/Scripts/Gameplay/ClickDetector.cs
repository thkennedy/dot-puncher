﻿using UnityEngine;
using DotFighter;

public class ClickDetector : MonoBehaviour
{
    public bool isActive = false;
    
    private bool isDown = false;

    // Update is called once per frame
    void Update()
    {

        if (!isActive)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (isDown)
                return;

            GameObject.Instantiate(Resources.Load("Prefabs/Particles/Punch"), Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 97)), Quaternion.identity);
            

            isDown = true;
            GameObject goPointedAt = RaycastObject(Input.mousePosition);

            if (goPointedAt == null)
                return;

            // get dot off object
            Dot dot = goPointedAt.GetComponent<Dot>();

            if (dot == null)
                return;

            GameController.instance.GetComponent<NetworkView>().RPC("ClickDot", RPCMode.All, dot.row, dot.column, UserData.playerUID);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            
            isDown = false;
        }

    }

    private GameObject RaycastObject(Vector2 screenPos)
    {
        RaycastHit info;
        #if UNITY_3_5
        Camera cam = Camera.mainCamera;
        #else
        Camera cam = Camera.main;
        #endif

        if (Physics.Raycast(cam.ScreenPointToRay(screenPos), out info, 200))
        {
            return info.collider.gameObject;
        }

        return null;
    }
}
