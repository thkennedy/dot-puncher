﻿using UnityEngine;
using System.Collections;

namespace DotFighter
{
    public enum GameStates
    {
        IDLE = 0,
        CONNECTING_TO_LOBBY,
        MATCHMAKING,
        WAITING_FOR_PLAYERS,
        GAME_STARTED,
        GAME_ENDED

    }
}