﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace DotFighter
{
    public class DotController : MonoBehaviour
    {
        public GameObject dotPrefab;

        public Dot[,] allDots;

        public int rows;

        public int columns;

        public float screenWidth;

        public float screenHeight;

        public float Z;

        public GameObject dotContainer;

        private DotSerialized[] serializedArray;

        // Use this for initialization
        void Start()
        {

            GameController.instance.RegisterForGameEventCallback(OnGameEnd, GameEvents.GameEnded);

            allDots = new Dot[rows, columns];
            serializedArray = new DotSerialized[rows * columns];

        }

        public void CreateDots()
        {
            // get screen width and height
            screenWidth = Screen.width;
            screenHeight = Screen.height;

            //create a bunch of dots and store them
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    // create
                    GameObject newDot = GameObject.Instantiate(dotPrefab) as GameObject;

                    newDot.transform.SetParent(dotContainer.transform);

                    Dot dot = newDot.GetComponent<Dot>();

                    Vector3 position = new Vector3(screenWidth * (j+1) / (columns+1), screenHeight * (i+4) / (rows+5.5f), Z);

                    newDot.transform.position = Camera.main.ScreenToWorldPoint(position);

                    newDot.transform.rotation = Random.rotation;

                    dot.whoClickedMeLast = "NoOne";

                    dot.row = i ;
                    dot.column = j ;

                    allDots[dot.row, dot.column] = dot;

                }
            }
        }

        public string GetStringSerializeDots()
        {
            string[] stringArray = new string[rows * columns];
            int k = 0;
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    stringArray[k++] = allDots[i, j].ToString();
                }
            }

            return SerializationHelper.SerializeToString(stringArray);
        }

        public byte[] GetByteSerializedDots()
        {
            int k = 0;
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    serializedArray[k++] = allDots[i, j].ToDotSerialized();
                }
            }

            return SerializationHelper.SerializeToByteArray(serializedArray);
        }

        public void ClaimDot(int claimRow, int claimColumn, string playerUID)
        {
        
            string whoClickedLast = allDots[claimRow, claimColumn].whoClickedMeLast;

            // claim dot and add point to person who claimed it.
            if (whoClickedLast != playerUID)
            {
                // if already owned, remove point from previous owner
                if (whoClickedLast != "NoOne")
                {
                    GameController.instance.playerScores[whoClickedLast] -= 1;
                    // if I lose a point
                    if (whoClickedLast == UserData.playerUID)
                        GameController.instance.soundController.OnPlayGetHit();
                }

                allDots[claimRow, claimColumn].whoClickedMeLast = playerUID;

                GameController.instance.playerScores[playerUID] += 1;

                if (playerUID == UserData.playerUID)
                    GameController.instance.soundController.OnPlayHitThing();
            }

            
            
        }

        public void OnGameEnd(object sender)
        {
            // hide all the dots
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    allDots[i, j].gameObject.SetActive(false);
                }
            }
        }

    }
}