﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace DotFighter
{
    [Serializable]
    public class DotSerialized
    {
        // const
        public DotSerialized() { }
        public DotSerialized(Dot sourceDot)
        {
            row = sourceDot.row;
            column = sourceDot.column;
            whoClickedMeLast = sourceDot.whoClickedMeLast;
        }
        public int row;

        // column
        public int column;

        // owner
        public string whoClickedMeLast;
    }

    public class Dot : MonoBehaviour
    {
        // owner
        public string whoClickedMeLast;

        public int row;

        // column
        public int column;

        public bool isDown;

        private DotSerialized scratchDotSerialized = new DotSerialized();

        public void AssignDotValues(DotSerialized sDot)
        {
            row = sDot.row;
            column = sDot.column;
            whoClickedMeLast = sDot.whoClickedMeLast;
        }

        public DotSerialized ToDotSerialized()
        {
            scratchDotSerialized.row = row;

            scratchDotSerialized.column = column;

            scratchDotSerialized.whoClickedMeLast = whoClickedMeLast;

            return scratchDotSerialized;
        }

        // Update is called once per frame
        void Update()
        {
            // update color
            if (!GameController.instance.players.ContainsKey(whoClickedMeLast))
                GetComponent<Renderer>().material.color = Color.gray;
            else
                GetComponent<Renderer>().material.color = GameController.instance.playerColors[ GameController.instance.playerColorIndexes[whoClickedMeLast]];
        }

        //public override string ToString()
        //{
        //    StringBuilder builder = new StringBuilder();

        //    builder.Append(row).Append(',').Append(column).Append(',').Append(whoClickedMeLast);

        //    string returnVal = builder.ToString();

        //    return returnVal;
        //}

        public void SetWhoClickedMeFromString(string serializedDot)
        {
            // Divide all by comma (remove empty strings)
            string[] tokens = serializedDot.Split(new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);

            whoClickedMeLast = tokens[2];
        }
    }

}