﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using DotFighter;
using UnityEngine.UI;
using System;

public enum NetworkMessages
{
    OnConnectedToServer,
    OnDisconnectedFromServer,
    OnFailedToConnect,
    OnNetworkInstantiate,
    OnPlayerConnected,
    OnPlayerDisconnected,
    OnSerializeNetworkView,
    OnServerInitialized,
    OnFailedToConnectToMasterServer,
    OnNetworkModeChanged
}


public class NetworkController : MonoBehaviour
{
    public delegate void MasterServerEventDelegate(MasterServerEvent result);
    public delegate void NetworkMessageReceived();
    public delegate void NetworkModeChanged();
    
    MasterServerEventDelegate RegistrationFailed;
    MasterServerEventDelegate HostListReceived;
    MasterServerEventDelegate RegistrationSuccess;

    NetworkMessageReceived onConnectedToServer;
    NetworkMessageReceived onDisconnectedFromServer;
    NetworkMessageReceived onFailedToConnect;
    NetworkMessageReceived onNetworkInstantiate;
    NetworkMessageReceived onPlayerConnected;
    NetworkMessageReceived onPlayerDisconnected;
    NetworkMessageReceived onSerializeNetworkView;
    NetworkMessageReceived onServerInitialized;
    NetworkMessageReceived onFailedToConnectToMasterServer;
    NetworkMessageReceived onNetworkModeChanged;
    

    public string masterServerIP = "52.34.45.183";
    public string masterServerPort = "23466";

    public NetworkReachability networkMode;

    // Use this for initialization
    void Awake()
    {

        Network.natFacilitatorIP = "52.34.45.183";
        Network.natFacilitatorPort = 50005;

        networkMode = Application.internetReachability;
        
    }

    // registration messages
    public void RegisterCallbackForMessage(NetworkMessageReceived callback, NetworkMessages messageType)
    {
        switch(messageType)
        {
            case NetworkMessages.OnConnectedToServer:
                onConnectedToServer -= callback;
                onConnectedToServer += callback;
                break;
            case NetworkMessages.OnDisconnectedFromServer:
                onDisconnectedFromServer -= callback;
                onDisconnectedFromServer += callback;
                break;
            case NetworkMessages.OnFailedToConnect:
                onFailedToConnect -= callback;
                onFailedToConnect += callback;
                break;
            case NetworkMessages.OnNetworkInstantiate:
                onNetworkInstantiate -= callback;
                onNetworkInstantiate += callback;
                break;  
            case NetworkMessages.OnPlayerConnected:
                onPlayerConnected -= callback;
                onPlayerConnected += callback;
                break;
            case NetworkMessages.OnPlayerDisconnected:
                onPlayerDisconnected -= callback;
                onPlayerDisconnected += callback;
                break;
            case NetworkMessages.OnSerializeNetworkView:
                onSerializeNetworkView -= callback;
                onSerializeNetworkView += callback;
                break;
            case NetworkMessages.OnServerInitialized:
                onServerInitialized -= callback;
                onServerInitialized += callback;
                break;
            case NetworkMessages.OnFailedToConnectToMasterServer:
                onFailedToConnectToMasterServer -= callback;
                onFailedToConnectToMasterServer += callback;
                break;
            case NetworkMessages.OnNetworkModeChanged:
                onNetworkModeChanged -= callback;
                onNetworkModeChanged += callback;
                break;  
            default:
                Debug.Log("Received Unknown Network Message Type: " + messageType.ToString());
                break;
        }
    }

    public void RegisterForServerEvent(MasterServerEventDelegate callback,MasterServerEvent serverEvent )
    {
        switch(serverEvent)
        {
            case MasterServerEvent.RegistrationFailedGameName:
            case MasterServerEvent.RegistrationFailedGameType:
            case MasterServerEvent.RegistrationFailedNoServer:
                RegistrationFailed += callback;
                break;

            case MasterServerEvent.RegistrationSucceeded:
                RegistrationSuccess += callback;
                break;

            case MasterServerEvent.HostListReceived:
                HostListReceived += callback;
                break;
        }
    }

    void OnMasterServerEvent(MasterServerEvent result)
    {
        switch(result)
        {
            case MasterServerEvent.RegistrationFailedGameName:
            case MasterServerEvent.RegistrationFailedGameType:
            case MasterServerEvent.RegistrationFailedNoServer:
                // pass along failure messages to those who have registered for them
                OnRegistrationFailure(result);
                break;

            case MasterServerEvent.RegistrationSucceeded:
                OnRegistrationSuccess(result);
                break;

            case MasterServerEvent.HostListReceived:
                OnHostListReceived(result);
                break;

            default:
                Debug.Log("Received a new or undefined message from master server: " + result.ToString());
                break;
        }
    }

    void OnConnectedToServer()
    {
        GameController.instance.InitPlayers();

        // send server my deets
        GameController.instance.GetComponent<NetworkView>().RPC("ProcessConnectedClient", RPCMode.Others ,UserData.playerUID, UserData.playerShortName);

        if (onConnectedToServer != null)
            onConnectedToServer();
    }

    void OnFailedToConnect()
    {
        if (onFailedToConnect != null)
            onFailedToConnect();
    }

    void OnDisconnectedFromServer()
    {
        if (onDisconnectedFromServer != null)
            onDisconnectedFromServer();
    }

    void OnNetworkInstantiate(NetworkMessageInfo info)
    {
        if (onNetworkInstantiate != null)
            onNetworkInstantiate();
    }

    void OnPlayerConnected(NetworkPlayer player)
    {
        GameController.instance.InitConnectedPlayerData(player);

        if (onPlayerConnected != null)
            onPlayerConnected();
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        if (onPlayerDisconnected != null)
            onPlayerDisconnected();
    }


    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (onSerializeNetworkView != null)
            onSerializeNetworkView();
    }

    void OnServerInitialized()
    {
        if (onServerInitialized != null)
            onServerInitialized();
    }

    void OnRegistrationSuccess(MasterServerEvent result)
    {
        // successfully created server, start timer to wait for others
        OnCreatedRoom();

        // invoke callbacks
        if (RegistrationSuccess != null)
            RegistrationSuccess(result);
    }

    void OnRegistrationFailure(MasterServerEvent result)
    {
        // call the callbacks that want to know about this
        if (RegistrationFailed != null)
            RegistrationFailed(result);
        
        // try again
       // BeginMatchmaking();
    }

    void OnFailedToConnectToMasterServer(NetworkConnectionError info)
    {
        if (onFailedToConnectToMasterServer != null)
            onFailedToConnectToMasterServer();

        Debug.LogWarning(info);
    }

    void OnHostListReceived(MasterServerEvent result)
    {
        // invoke callbacks
        if (HostListReceived != null)
            HostListReceived(result);
    }

    public void BeginMatchmaking()
    {
        GameController.instance.gameState = GameStates.MATCHMAKING;

        // set master server's public ip
        MasterServer.ipAddress = masterServerIP;
        MasterServer.port = 23466;

        // any games up?
        MasterServer.RequestHostList(GameController.instance.gameType);
        
    }

    public void RegisterServer(string gameType, int numPlayers)
    {
        // initialize as a server with 8 players, port 25002 (its in the docs) and likely using NAT
        if (networkMode != NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            GameController.instance.secondsToWaitForPlayers = 1;
            numPlayers = 0;
        }
            
        Network.InitializeServer(numPlayers, 25002, !Network.HavePublicAddress());
        

        // register as a host with the master server so others can join
        // for future friend-filtered match making, have to be logged into FB
        // anonymous means random plays only.
        if (FB.IsLoggedIn)
        {
            MasterServer.RegisterHost(gameType, UserData.playerFirstName + UserData.playerLastName + UnityEngine.Random.Range(0, 9999).ToString(), "DotFighterBase");
        }
        else
        {
            string gameName = Guid.NewGuid().ToString();
            MasterServer.RegisterHost(gameType, gameName, GameController.instance.gameType);
        }
    }


    void OnCreatedRoom()
    {
        
    }

    void ProcessNetworkModeChange(NetworkReachability newMode)
    {
        networkMode = newMode;
        
        if (onNetworkModeChanged != null)
            onNetworkModeChanged();

    }

    IEnumerator CheckForWifiChanges()
    {
        NetworkReachability newData = Application.internetReachability;

        if (newData != networkMode)
        {
            ProcessNetworkModeChange(newData);
        }

        yield return new WaitForSeconds(1.0f);
    }
}
