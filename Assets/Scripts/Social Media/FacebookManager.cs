﻿using UnityEngine;
using System.Collections;
using Facebook;
using System.Collections.Generic;
using System;

public enum FBCallbackType
{
    INIT_COMPLETE,
    LOGIN_COMPLETE,
    PERMISSIONSCHECK_COMPLETE,
    DATAQUERY_COMPLETE
}

public class FacebookManager : MonoBehaviour
{
    // delegate the UI can register to for init stuff
    public delegate void FBMgrDelegateNoParams();

    private FBMgrDelegateNoParams fbInitDelegate;

    private FBMgrDelegateNoParams fbLoginCompleteDelegate;

    private FBMgrDelegateNoParams fbPermissionsCompleteDelegate;

    private FBMgrDelegateNoParams fbPlayerDataQueryCompleteDelegate;

    // permissions
    public bool email = false;
    public bool publicProfile = false;
    public bool userFriends = false;

    void Awake()
    {
        FB.Init(OnInitComplete);

        // fb manager needs to stay through game
        DontDestroyOnLoad(gameObject);
    }

    // callback registration
    public void RegisterForCallback(FBCallbackType type, FBMgrDelegateNoParams callback)
    {
        switch(type)
        {
            case FBCallbackType.DATAQUERY_COMPLETE:
                fbPlayerDataQueryCompleteDelegate -= callback;
                fbPlayerDataQueryCompleteDelegate += callback;
                break;

            case FBCallbackType.INIT_COMPLETE:
                fbInitDelegate -= callback;
                fbInitDelegate += callback;
                break;

            case FBCallbackType.LOGIN_COMPLETE:
                fbLoginCompleteDelegate -= callback;
                fbLoginCompleteDelegate += callback;
                break;

            case FBCallbackType.PERMISSIONSCHECK_COMPLETE:
                fbPermissionsCompleteDelegate -= callback;
                fbPermissionsCompleteDelegate += callback;
                break;
        }
    }
   
    void OnInitComplete()
    {
        if (fbInitDelegate != null)
            fbInitDelegate();
    }

    public void FBLogin()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("email, public_profile, user_friends", FBLoginCallback);
        }
    }

    public void FBLogout()
    {
        if (FB.IsLoggedIn)
        {
            FB.Logout();
        }

        // set all pertinents to false and flush fb userdata
        UserData.RemoveFacebookData();
    }

    private void FBLoginCallback(FBResult result)
    {
        // call other methods that registered to be notified
        if (fbLoginCompleteDelegate != null)
            fbLoginCompleteDelegate();
    }

    public void CheckGrantedPermissions()
    {
        FB.API("me/permissions", HttpMethod.GET, ParseGrantedPermissions);
    }

    void ParseGrantedPermissions(FBResult response)
    {
        if (!string.IsNullOrEmpty(response.Error))
        {
            Debug.LogWarning(response.Error);

            if (fbPermissionsCompleteDelegate != null)
                fbPermissionsCompleteDelegate();

            return;
        }

        Hashtable data = response.Text.hashtableFromJson();

        // get data from the hastable
        ArrayList permissions = (ArrayList)data["data"];

        foreach (Hashtable item in permissions)
        {
            
            switch ((string)item["permission"])
            {
                case "email":
                    if ((string)item["status"] == "granted")
                    {
                        Debug.Log("Has Email Permissions");
                        UserData.isFacebookLinked = true;
                        email = true;
                    }
                    break;
                case "public_profile":
                    if ((string)item["status"] == "granted")
                    {
                        Debug.Log("Has Public Profile Permissions");
                        publicProfile = true;
                        UserData.isFacebookLinked = true;
                    }
                    break;
                case "user_friends":
                    if ((string)item["status"] == "granted")
                    {
                        Debug.Log("Has User Friends Permissions");
                        UserData.isFacebookLinked = true;
                        userFriends = true;
                    }
                    break;
            }
        }

        if (fbPermissionsCompleteDelegate != null)
            fbPermissionsCompleteDelegate();
    }

    public void GetPlayerData()
    {
        FB.API("me", HttpMethod.GET, OnPlayerDataQueryComplete);
        FB.API("me/picture?redirect=false", HttpMethod.GET, OnPlayerDataQueryComplete);
    }

    private void OnPlayerDataQueryComplete(FBResult result)
    {
        Hashtable data = result.Text.hashtableFromJson();
        

        if (data.ContainsKey("data"))
        {
            Hashtable newData = data["data"] as Hashtable;
            UserData.playerFBPicURL = (string)newData["url"];
            return;
        }

        // get dat data
        UserData.playerFBID = (string)data["id"];

        UserData.playerFirstName = (string)data["first_name"];

        UserData.playerLastName = (string)data["last_name"];

        UserData.playerFullName = UserData.playerFirstName + " " + UserData.playerLastName;

        UserData.playerFBLink = (string)data["link"];

        UserData.playerTimeZone = Convert.ToInt32(data["timezone"]);

        UserData.playerShortName = UserData.playerFirstName + " " + UserData.playerLastName.Substring(0,1) + ".";

        if (fbPlayerDataQueryCompleteDelegate != null)
            fbPlayerDataQueryCompleteDelegate();
    }


}
