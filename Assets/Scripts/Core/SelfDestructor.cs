﻿using UnityEngine;
using System.Collections;

public class SelfDestructor : MonoBehaviour
{
    public int timeUntilSelfDestruct = 1;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("DestructAfterTime");
    }

    IEnumerator DestructAfterTime()
    {
        yield return new WaitForSeconds(timeUntilSelfDestruct);

        DestroyObject(gameObject);
    }
}
