﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
    public Image screenFader;

    public float fadeTime;

    public int displayTime;

    void Awake()
    {
        
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine("Timer");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Timer()
    {
        // start fade
        screenFader.CrossFadeAlpha(0.0f, fadeTime, true);
        yield return new WaitForSeconds(fadeTime);

        yield return new WaitForSeconds(displayTime);

        screenFader.CrossFadeAlpha(1.0f, fadeTime, true);

        yield return new WaitForSeconds(fadeTime);

        Application.LoadLevel("TitleScene");

    }
}
