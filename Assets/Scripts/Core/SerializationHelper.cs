﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System; // We will need the Serializable Attribute from here
using System.IO; // This line will enable you the usage of MemoryStreams
using System.Runtime.Serialization.Formatters.Binary; // Finally we add BinaryFormatters

public static class SerializationHelper
{
    private static StringBuilder builder = new StringBuilder();

    public static string SerializeToString(Dictionary<string, string> dict)
    {
        // all pairs - if it doesn't have a .ToString() method this won't work
        foreach(KeyValuePair<string,string> pair in dict)
        {
            builder.Append(pair.Key).Append(":").Append(pair.Value).Append(',');
        }

        // dump into return value
        string returnVal = builder.ToString();

        // clear out stringbuilder
        builder.Remove(0, builder.Length);

        // trim final comma
        returnVal.TrimEnd(',');
        

        return returnVal;
    }

    public static string SerializeToString(Dictionary<string, int> dict)
    {
        // all pairs - if it doesn't have a .ToString() method this won't work
        foreach (KeyValuePair<string, int> pair in dict)
        {
            builder.Append(pair.Key).Append(":").Append(pair.Value).Append(',');
        }

        // dump into return value
        string returnVal = builder.ToString();

        // clear out stringbuilder
        builder.Remove(0, builder.Length);

        // trim final comma
        returnVal.TrimEnd(',');


        return returnVal;
    }

    public static string SerializeToString(string[] stringArray)
    {
        // all pairs - if it doesn't have a .ToString() method this won't work
        foreach (string entry in stringArray)
        {
            builder.Append(entry).Append(":");
        }

        // dump into return value
        string returnVal = builder.ToString();

        // clear out stringbuilder
        builder.Remove(0, builder.Length);

        // trim final comma
        returnVal.TrimEnd(':');

        return returnVal;
    }

    public static byte[] SerializeToByteArray(DotFighter.DotSerialized[] dotArray)
    {
        BinaryFormatter binFormatter = new BinaryFormatter(); // Create Formatter and Stream to process our data
        MemoryStream memStream = new MemoryStream();

        binFormatter.Serialize(memStream, dotArray);

        byte[] returnVal = memStream.ToArray();

        memStream.Close();

        return returnVal;

    }

    public static void DeserializeFromByteArray(byte[] byteArray, ref DotFighter.DotSerialized[] dotArray)
    {
        BinaryFormatter binFormatter = new BinaryFormatter(); // Create Formatter and Stream to process our data
        MemoryStream memStream = new MemoryStream();

        /* This line will write the byte data we received into the stream The second parameter specifies the offset, since we want to start at the beginning of the stream we set this to 0.
         * The third   parameter specifies the maximum number of bytes to be written into the stream, so we use the amount of bytes that our data contains by passing the length of our byte array. */
        memStream.Write(byteArray, 0, byteArray.Length);

        /* After writing our data, our streams internal "reader" is now at the last position of the stream. We shift it back to the beginning of our stream so we can start reading from the very    
         *  beginning */
        memStream.Seek(0, SeekOrigin.Begin);

        dotArray = (DotFighter.DotSerialized[])binFormatter.Deserialize(memStream);

        memStream.Close();
    }

    public static void DeserializeFromString(string serializedData, ref string[] returnStringArray)
    {
        returnStringArray = null;

        // Divide all pairs (remove empty strings)
        string[] tokens = serializedData.Split(new char[] { ':'},
            StringSplitOptions.RemoveEmptyEntries);

        // size the array
        returnStringArray = new string[tokens.Length];

        // Walk through each item
        for (int i = 0; i < tokens.Length; i += 2)
        {
            returnStringArray[i] = tokens[i];

        }

    }

    

    public static void DeserializeFromString(string serializedDict, Dictionary<string,string> returnDict)
    {
        returnDict.Clear();

        // Divide all pairs (remove empty strings)
        string[] tokens = serializedDict.Split(new char[] { ':', ',' },
            StringSplitOptions.RemoveEmptyEntries);

        // Walk through each item
        for (int i = 0; i < tokens.Length; i += 2)
        {
            string key = tokens[i];
            string value = tokens[i + 1];

            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                returnDict.Add(key, value);
            else
                Debug.LogWarning("One of these is null: " + key + " : " + value);
        }

    }

    public static void DeserializeFromString(string serializedDict, Dictionary<string, int> returnDict)
    {
        returnDict.Clear();

        // Divide all pairs (remove empty strings)
        string[] tokens = serializedDict.Split(new char[] { ':', ',' },
            StringSplitOptions.RemoveEmptyEntries);

        // Walk through each item
        for (int i = 0; i < tokens.Length; i += 2)
        {
            string key = tokens[i];
            int value = Convert.ToInt32(tokens[i + 1]);

            if (!string.IsNullOrEmpty(key))
                returnDict.Add(key, value);
            else
                Debug.LogWarning("One of these is null: " + key + " : " + value);
        }
    }
}
