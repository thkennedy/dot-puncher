﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{
    public AudioClip[] hitThingSounds;
    public AudioClip[] getHitSounds;
    public AudioClip titleMusic;
    public AudioClip loadMusic;
    public AudioClip gameplayMusic;

    public AudioSource _audioSourceAmbient;
    public AudioSource _audioSourceFX;

    void Awake()
    {
        if (_audioSourceAmbient == null)
            _audioSourceAmbient = gameObject.AddComponent<AudioSource>();


        if (_audioSourceFX == null)
            _audioSourceFX = gameObject.AddComponent<AudioSource>();

        // check user data to see if we should be muted
        MuteSound(!UserData.soundIsOn);
            
    }

    public void OnPlayHitThing()
    {
        _audioSourceFX.loop = false;
        _audioSourceFX.PlayOneShot(hitThingSounds[UnityEngine.Random.Range(0, hitThingSounds.Length - 1)], 1f);
    }

    public void OnPlayGetHit()
    {
        _audioSourceFX.loop = false;
        _audioSourceFX.PlayOneShot(getHitSounds[UnityEngine.Random.Range(0, getHitSounds.Length - 1)], 1f);
    }

    public void PlayTitleMusic(bool shouldPlay)
    {
        if (shouldPlay)
        {
            _audioSourceAmbient.volume = 1.0f;

            if (titleMusic != null)
            {
                _audioSourceAmbient.loop = true;
                _audioSourceAmbient.clip = titleMusic;
                _audioSourceAmbient.Play();
            }
        }
        else
        {
            _audioSourceAmbient.loop = false;
            _audioSourceAmbient.Stop();
        }
    }

    public void PlayGameplayMusic(bool shouldPlay)
    {
        if (shouldPlay)
        {
            _audioSourceAmbient.volume = 0.25f;
            if (gameplayMusic != null)
            {
                _audioSourceAmbient.loop = false;
                _audioSourceAmbient.clip = gameplayMusic;
                _audioSourceAmbient.Play();
            }
        }
        else
        {
            _audioSourceAmbient.Stop();
            _audioSourceAmbient.loop = false;
            
        }
    }

    public void StopMusic()
    {
        _audioSourceAmbient.Stop();
    }

    public void MuteSound(bool isMuted)
    {
        _audioSourceAmbient.mute = isMuted;
        _audioSourceFX.mute = isMuted;
    }


}
