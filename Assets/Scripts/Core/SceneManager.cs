﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneManager : MonoBehaviour
{
    private FacebookManager _fbManager;
    public FacebookManager fbManager
    {
        get
        {
            if (_fbManager == null)
                _fbManager = GetComponent<FacebookManager>();

            return _fbManager;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
            {
                _soundController = FindObjectOfType<SoundController>();
                if (_soundController == null)
                {
                    GameObject go = GameObject.Instantiate(Resources.Load("Prefabs/SoundController")) as GameObject;
                    _soundController = go.GetComponent<SoundController>();
                }
            }

            return _soundController;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            // create the singleton on first awake
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            // if we've accidentally made two game controllers
            if (this != _instance)
                Destroy(this.gameObject);
        }

        CreateControllers();

        // don't let screen go landscape
        Screen.autorotateToPortrait = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeRight = false;
        Screen.orientation = ScreenOrientation.Portrait;
    }

    void Start()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
         {
             System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
         } 
    }

    private void CreateControllers()
    { 
        if (_fbManager == null)
        {
            _fbManager = gameObject.AddComponent<FacebookManager>();
        }

    }

    public void LoadMainGame()
    {
        Application.LoadLevel("BaseLevel");
        soundController.PlayTitleMusic(false);
    }

    private static SceneManager _instance;
    public static SceneManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SceneManager>();

                if (_instance != null)
                    DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }
    

}
