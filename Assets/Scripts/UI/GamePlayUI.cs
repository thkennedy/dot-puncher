﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

namespace DotFighter
{
    public class PlayerScore
    {
        public string playerName;
        public string playerID;
        public int playerScore;
    }
    public class GamePlayUI : MonoBehaviour
    {
        // matchmaking status UI
        public RectTransform matchMakingStatusPanel;
        Text matchMakingStatusText;
        public Button cancelMatchmakingButton;
        public RectTransform confirmMatchmakingCancelPanel;
        public Button confirmCancelMatchmakingButton;
        public Button continueMatchmakingButton;
        
        public float joinTimeLeft
        {
            get
            {
                return GameController.instance.joinTimeLeft;
            }

            set
            {
                GameController.instance.joinTimeLeft = value;
            }
        }

        // connection failure UI
        public RectTransform connectionFailurePanel;
        public Button connectionFailureOKButton;
        public Text connectionFailureText;

        // end game UI
        public RectTransform endgamePanel;
        public Text placeText;

        public ScoreDisplay[] playerScoreCards;

        public FinalScoreEntry[] finalScoreEntries;

        bool hasRegistered = false;

        

        // Use this for initialization
        void Start()
        {
            // register for callbacks
            GameController.instance.RegisterForServerEvent(HostListReceived, MasterServerEvent.HostListReceived);
            GameController.instance.RegisterForServerEvent(ServerRegistrationSuccess, MasterServerEvent.RegistrationSucceeded);
            GameController.instance.RegisterForGameEventCallback(GameEnded, GameEvents.GameEnded);
            GameController.instance.RegisterForGameEventCallback(PlayerListUpdated, GameEvents.PlayerListUpdated);
            GameController.instance.RegisterForGameEventCallback(GameStarted, GameEvents.GameStarted);
            

            GameController.instance.networkController.RegisterCallbackForMessage(OnNewPlayerConnected, NetworkMessages.OnPlayerConnected);
            GameController.instance.networkController.RegisterCallbackForMessage(OnSuccessfulConnectionToServer, NetworkMessages.OnConnectedToServer);
            GameController.instance.networkController.RegisterCallbackForMessage(OnFailToConnectToMatchMaking, NetworkMessages.OnFailedToConnectToMasterServer);
            GameController.instance.networkController.RegisterCallbackForMessage(FailToConnectToServer, NetworkMessages.OnFailedToConnect);
            GameController.instance.networkController.RegisterCallbackForMessage(LostConnectionToServer, NetworkMessages.OnDisconnectedFromServer);

            // set status panel visible
            matchMakingStatusPanel.gameObject.SetActive(true);
            // get panel's text components
            Text[] textComponents = matchMakingStatusPanel.GetComponentsInChildren<Text>();
            
            matchMakingStatusText = textComponents[1];
            SetStatusText("Connecting to Matchmaking Service...");

            // button inits
            cancelMatchmakingButton.onClick.AddListener(OnTryCancelMatchmaking);
            confirmCancelMatchmakingButton.onClick.AddListener(OnCancelMatchmaking);
            continueMatchmakingButton.onClick.AddListener(OnContinueMatchmaking);
            connectionFailureOKButton.onClick.AddListener(OnCancelMatchmaking);

            // panel inits
            connectionFailurePanel.gameObject.SetActive(false);
            confirmMatchmakingCancelPanel.gameObject.SetActive(false);
            endgamePanel.gameObject.SetActive(false);
            
            
        }       

        public void SetStatusText(string text)
        {
            matchMakingStatusText.text = text;
        }

        void ServerRegistrationSuccess(MasterServerEvent result)
        {
            if (!hasRegistered)
            {
                StartCoroutine("WaitForPlayers", GameController.instance.secondsToWaitForPlayers);
                hasRegistered = true;
            }
        }

        void OnCancelMatchmaking()
        {
            Application.LoadLevel("TitleScene");

        }

        void OnTryCancelMatchmaking()
        {
            confirmMatchmakingCancelPanel.gameObject.SetActive(true);
        }

        void OnContinueMatchmaking()
        {
            confirmMatchmakingCancelPanel.gameObject.SetActive(false);
        }

        void GameStarted(Object sender)
        {
            matchMakingStatusPanel.gameObject.SetActive(false);
        }

        void GameEnded(Object sender)
        { 
            // show endgame control
            endgamePanel.gameObject.SetActive(true);

            ShowFinalScores();
        }

        void OnNewPlayerConnected()
        {
            // if I'm the server, send out time left
            if (Network.isServer)
                GameController.instance.GetComponent<NetworkView>().RPC("UpdateJoinTime", RPCMode.All, joinTimeLeft);

        }

        void OnSuccessfulConnectionToServer()
        {
            StartCoroutine("WaitForPlayers", GameController.instance.secondsToWaitForPlayers);
        }

        void OnFailToConnectToMatchMaking()
        {
            connectionFailurePanel.gameObject.SetActive(true);
            connectionFailureText.text = "Unable to connect to matchmaking server. It may be down for maintenance, or you may need to close and restart the app.";
        }

        void FailToConnectToServer()
        {
            connectionFailurePanel.gameObject.SetActive(true);
            connectionFailureText.text = "Failed to connect to chosen game - hit ok to go back to the title screen and try again.";
        }

        void LostConnectionToServer()
        {
            if (GameController.instance.gameState != GameStates.GAME_ENDED)
            {
                connectionFailurePanel.gameObject.SetActive(true);
                connectionFailureText.text = "Server player quit the game - hit ok to go back to the title screen and find another match.";
            }
        }

        void PlayerListUpdated(Object sender)
        {
            ClearScoreCards();

            List<string> playerUIDs = new List<string>( GameController.instance.players.Keys);
            // build scorecards
            for(int i = 0; i < GameController.instance.numPlayers; ++i)
            {
                playerScoreCards[i].SetData(playerUIDs[i]);
                finalScoreEntries[i].SetData(playerUIDs[i]);
            }
        }

        void ClearScoreCards()
        {
            for(int i = 0; i < 8; ++i)
            {
                playerScoreCards[i].ClearData();
                finalScoreEntries[i].ClearData();
            }
        }

        public IEnumerator WaitForPlayers(float waitTime)
        {
            GameController.instance.dotController.CreateDots();

            joinTimeLeft = waitTime;

            while (joinTimeLeft > 0)
            {
                SetStatusText("Waiting for Players...\n" + joinTimeLeft.ToString());
                joinTimeLeft -= 1.0f;

                //if (Network.isServer)
                //    GameController.instance.networkView.RPC("UpdateJoinTime", RPCMode.Others, joinTimeLeft-1);

                yield return new WaitForSeconds(1.0f);
            }

            GameController.instance.soundController.PlayGameplayMusic(true);
            SetStatusText("FIGHT!!!");
            yield return new WaitForSeconds(1.0f);


            // if we're the server, start the game because timer is over
            if (Network.isServer)
            {
                // make status panel disappear
                GameController.instance.GetComponent<NetworkView>().RPC("StartGame", RPCMode.All);
            }
        }

        public void SetPlaceText(int place)
        {
            string text = null;
            switch(place)
            {
                case 0:
                    text = "First";
                    break;
                case 1:
                    text = "Second";
                    break;
                case 2:
                    text = "Third";
                    break;
                case 3:
                    text = "Fourth";
                    break;
                case 4:
                    text = "Fifth";
                    break;
                case 5:
                    text = "Sixth";
                    break;
                case 6:
                    text = "Seventh";
                    break;
                case 7:
                    text = "Eighth";
                    break;
            }

            placeText.text = text + " Place";
        }

        void HostListReceived(MasterServerEvent result)
        {
            SetStatusText("Server Reached, getting game list");
        }

        public void ShowAllScoreCardsCards()
        {
            endgamePanel.gameObject.SetActive(true);
        }

        public void HideAllScoreCards()
        {
            endgamePanel.gameObject.SetActive(false);
        }

        public void ShowFinalScores()
        {
            // get all scores from gamecontroller
            List<KeyValuePair<string, int>> playerScores = GameController.instance.playerScores.ToList();

            playerScores.Sort((firstPair, nextPair) =>
                {
                    return firstPair.Value.CompareTo(nextPair.Value);
                }
            );

            for(int i = 0; i < playerScores.Count; ++i)
            {
                if (playerScores[i].Key == UserData.playerUID)
                {
                    SetPlaceText(7-i);
                    break;
                }

            }

            foreach (FinalScoreEntry score in finalScoreEntries)
            {
                score.SetScore();
            }

            endgamePanel.gameObject.SetActive(true);
        }
    }
}