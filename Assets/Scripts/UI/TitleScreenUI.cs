﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class TitleScreenUI : MonoBehaviour
{
    public RectTransform firstTimeScreen;
    public Button fbLoginButton;
    public Button anonymousLoginButton;
    public Button cancelAnonButton;
    public Button maskUpButton;
    public Button fightButton;
    public Button optionsButton;
    public Text anonWarningText;
    public OptionPanel optionsPanel;
    public Button aiDifficultyButton;
    public Text aiDifficultyText;
    public RectTransform wifiWarningPanel;
    public Button wifiWarningOKButton;
    public bool hasShownWifiWarning = false;
    public Text connectionWarningText;
    public Button howToPlayButton;
    public RectTransform tutorialPanel;
    public Button tutorialExitButton;
    public Button tutorialOKButton;

    void Awake()
    {
        if (SceneManager.instance == null)
            GameObject.Instantiate(Resources.Load("Prefabs/SceneManager"));
        
        // unityads init
        Advertisement.Initialize("131624235");
        
        // wait for init to complete
        SceneManager.instance.fbManager.RegisterForCallback(FBCallbackType.INIT_COMPLETE,OnInitComplete);
        SceneManager.instance.fbManager.RegisterForCallback(FBCallbackType.PERMISSIONSCHECK_COMPLETE, OnPermissionsCheckComplete);
        SceneManager.instance.fbManager.RegisterForCallback(FBCallbackType.LOGIN_COMPLETE, OnFBLoginComplete);
        fightButton.onClick.AddListener(SceneManager.instance.LoadMainGame);
        anonymousLoginButton.onClick.AddListener(OnAnonLoginHit);
        maskUpButton.onClick.AddListener(OnMaskUpHit);
        cancelAnonButton.onClick.AddListener(OnCancelAnonHit);
        optionsButton.onClick.AddListener(OnOptionsHit);
        wifiWarningOKButton.onClick.AddListener(OnDismissWifiWarning);
        aiDifficultyButton.onClick.AddListener(OnAIDifficultyClicked);
        tutorialExitButton.onClick.AddListener(OnDismissTutorial);
        tutorialOKButton.onClick.AddListener(OnDismissTutorial);
        howToPlayButton.onClick.AddListener(OnShowTutorial);

        tutorialPanel.gameObject.SetActive(false);
        wifiWarningPanel.gameObject.SetActive(false);
        optionsPanel.gameObject.SetActive(false);

        SceneManager.instance.soundController.PlayTitleMusic(true);

    }
    
    void OnInitComplete()
    {
        CheckInternetConnectMethod();

        // if we've played before, check pertinents
        if (UserData.hasPlayedBefore)
        {
            // using fb?
            if (UserData.isFacebookLinked)
            {
                // see if we can get permissions
                SceneManager.instance.fbManager.CheckGrantedPermissions();

            }
        }
        // if we haven't played before show first time screen
        else
        {
            ShowFirstTimeScreen(true);
            OnShowTutorial();
            UserData.hasPlayedBefore = true;
        }
    }

    void CheckInternetConnectMethod()
    {
        NetworkReachability status = Application.internetReachability;
        switch(status)
        {
            case NetworkReachability.ReachableViaCarrierDataNetwork:
                if (hasShownWifiWarning == false)
                {
                    wifiWarningPanel.gameObject.SetActive(true);
                    connectionWarningText.text = "To play Dot-Fighter competitively, you must be connected to wi-fi. Mobile carrier data latency doesn't allow for a fair gameplay experience. You can still practice against AI until you can connect to wi-fi.";
                    hasShownWifiWarning = true;
                   
                }
                break; 
            case NetworkReachability.NotReachable:
                wifiWarningPanel.gameObject.SetActive(true);
                connectionWarningText.text = "Dot Fighter requires a wi-fi internet connection to play competitively! If you are in airplane mode, you can play locally against AI opponents to sharpen your skills!";
                break;
        }

    }

    void OnDismissWifiWarning()
    {
        wifiWarningPanel.gameObject.SetActive(false);
    }

    void OnAIDifficultyClicked()
    {
        UserData.difficulty++;
        UpdateDifficultyText();
    }

    void OnDismissTutorial()
    {
        tutorialPanel.gameObject.SetActive(false);
    }

    void OnShowTutorial()
    {
        tutorialPanel.gameObject.SetActive(true);
    }

    void UpdateDifficultyText()
    {
        switch(UserData.difficulty)
        {
            case 0:
                aiDifficultyText.text = "AI Difficulty: Easy";
                break;
            case 1:
                aiDifficultyText.text = "AI Difficulty: Medium";
                break;
            case 2:
                aiDifficultyText.text = "AI Difficulty: Hard";
                break;
            case 3:
                aiDifficultyText.text = "AI Difficulty: Insane";
                break;
            default:
                aiDifficultyText.text = "AI Difficulty: Easy";
                break;
        }
    }

    public void ShowFirstTimeScreen(bool isVisible)
    {
        firstTimeScreen.gameObject.SetActive(isVisible);
        
        // set all stuff to default state just in case
        ShowLoginButtons(true);
        ShowWarningOptions(false);
    }

    public void OnFBLoginHit()
    {
        SceneManager.instance.fbManager.FBLogin();
    }

    public void OnAnonLoginHit()
    {
        // show warning
        ShowLoginButtons(false);
        ShowWarningOptions(true);
    }

    private void OnMaskUpHit()
    {
        UserData.playerShortName = "Masked Fighter";
        UserData.isFacebookLinked = false;
        ShowFirstTimeScreen(false);
    }

    private void OnCancelAnonHit()
    {
        ShowFirstTimeScreen(true);
    }

    private void OnOptionsHit()
    {
        UpdateDifficultyText();
        optionsPanel.gameObject.SetActive(true);
    }

    private void ShowLoginButtons(bool isVisible)
    {
        fbLoginButton.gameObject.SetActive(isVisible);
        anonymousLoginButton.gameObject.SetActive(isVisible);
    }

    private void ShowWarningOptions(bool isVisible)
    {
        anonWarningText.gameObject.SetActive(isVisible);
        cancelAnonButton.gameObject.SetActive(isVisible);
        maskUpButton.gameObject.SetActive(isVisible);
    }

    public void OnFBLoginComplete()
    {
        // login complete, check the permissions I have
        SceneManager.instance.fbManager.CheckGrantedPermissions();
    }

    public void OnPermissionsCheckComplete()
    {
        // must have at least public profile to work, otherwise go anon
        if (!SceneManager.instance.fbManager.publicProfile)
        {
            // do error code 
            UserData.playerShortName = "Masked Fighter";
            //ShowFirstTimeScreen(true);
        }
        else
        {
            // hide the first time panel and grab player's details
            ShowFirstTimeScreen(false);
            SceneManager.instance.fbManager.GetPlayerData();
            UserData.isFacebookLinked = true;
        }
    }

    private void OnPlayerDataQueryComplete()
    {
        // data query complete get the avail data from userdata
    }

}
