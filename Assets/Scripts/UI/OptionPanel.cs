﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionPanel : MonoBehaviour
{
    public Button facebookButton;
    public Button soundOnButton;
    public Button soundMutedButton;
    public Text facebookLoginText;
    public Text facebookLogoutText;
    public Button exitButton;
    

    // Use this for initialization
    void Start()
    {
        facebookButton.onClick.AddListener(OnFacebookButtonClicked);
        soundOnButton.onClick.AddListener(SoundOff);
        soundMutedButton.onClick.AddListener(SoundOn);
        exitButton.onClick.AddListener(OnExitButtonClicked);

        // set state of fb Text
        SetFBLoginText();
        SetSoundIcon();

        SceneManager.instance.fbManager.RegisterForCallback(FBCallbackType.PERMISSIONSCHECK_COMPLETE,SetFBLoginText);
    }

    private void SoundOn()
    {
        UserData.soundIsOn = true;
        soundOnButton.gameObject.SetActive(true);
        soundMutedButton.gameObject.SetActive(false);
        SceneManager.instance.soundController.MuteSound(false);
    }

    private void SoundOff()
    {
        UserData.soundIsOn = false;
        soundOnButton.gameObject.SetActive(false);
        soundMutedButton.gameObject.SetActive(true);
        SceneManager.instance.soundController.MuteSound(true);
    }

    private void SetSoundIcon()
    {
        if (UserData.soundIsOn)
        {
            soundOnButton.gameObject.SetActive(true);
            soundMutedButton.gameObject.SetActive(false);
        }
    }

    private void OnFacebookButtonClicked()
    {
        // if we are linked, log out
        if (UserData.isFacebookLinked)
        {
            SceneManager.instance.fbManager.FBLogout();
            SetFBLoginText();
        }
        else
        {
            SceneManager.instance.fbManager.FBLogin();
        }
    }

    private void OnExitButtonClicked()
    {
        gameObject.SetActive(false);
    }

    private void SetFBLoginText()
    {
        // if we are linked, show log out
        if (UserData.isFacebookLinked)
        {
            facebookLoginText.gameObject.SetActive(false);
            facebookLogoutText.gameObject.SetActive(true);
        }
        else
        {
            facebookLoginText.gameObject.SetActive(true);
            facebookLogoutText.gameObject.SetActive(false);
        }
    }
}
