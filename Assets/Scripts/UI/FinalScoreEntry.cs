﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace DotFighter
{
    public class FinalScoreEntry : MonoBehaviour
    {
        public string playerUID;

        public Image playerColor;

        public Text playerName;

        public Text playerScore;

        public void SetData(string newUID)
        {
            playerUID = newUID;
            playerColor.color = GameController.instance.playerColors[ GameController.instance.playerColorIndexes[playerUID]];
            playerName.text = GameController.instance.players[playerUID];
        }

        public void ClearData()
        {
            playerUID = null;
            playerScore.text = "0";
        }

        public void SetScore()
        {
            playerScore.text = GameController.instance.playerScores[playerUID].ToString();
        }
    }
}