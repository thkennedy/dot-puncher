﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace DotFighter
{
    public class ScoreDisplay : MonoBehaviour
    {
        public string playerUID;

        // actual score display
        Text scoreText;
        // image color
        Image scoreCardColor;


        // Use this for initialization
        void Start()
        {
            // get components
            scoreText = GetComponentInChildren<Text>();

            scoreCardColor = GetComponentInChildren<Image>();
            
        }

        // Update is called once per frame
        void Update()
        {
            if (!string.IsNullOrEmpty(playerUID))
                scoreText.text = GameController.instance.playerScores[playerUID].ToString();
        }

        public void ShowCard(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        public void SetData(string newUID)
        {
            playerUID = newUID;

            Color setColor = GameController.instance.playerColors[GameController.instance.playerColorIndexes[playerUID]];
            scoreCardColor.color = setColor;
        }

        public void ClearData()
        {
            playerUID = null;
        }

    }
}